# SMILEMETER


### Description du projet

Application mobile développé avec le framework Flutter qui permet de calculer la moyenne de satisfaction en prenant une photo d'un groupe de personnes.
Cette application utilise Firebase, le module Flare pour les animations, ainsi que l'API Google Vision pour la reconnaissance faciale.


### Récupérer les dépendances :

`flutter packages get`

### Mettre à jour les dépendances

`flutter packages upgrade`

