import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';

import 'face_widget.dart';
import 'auth.dart';
import 'face_class.dart';

class FaceCapture extends StatefulWidget {

  FaceCapture({this.auth, this.onSignedOut});
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  @override
  _FaceCaptureState createState() => _FaceCaptureState();

}

class _FaceCaptureState extends State<FaceCapture> {

  String _title;
  int _cameraFace = FlutterMobileVision.CAMERA_FRONT;
  bool _autoFocusFace = true;
  bool _torchFace = false;
  bool _multipleFace = true;
  bool _showTextFace = false;
  Size _previewFace;
  List<SmileyFace> _faces = [];

  @override
  void initState() {
    super.initState();
    FlutterMobileVision.start().then((previewSizes) => setState(() {
      _previewFace = previewSizes[_cameraFace].first;
    }));
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text('Smiley'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Deconnexion', style: new TextStyle(fontSize: 17.0, color: Colors.white)),
              onPressed: _signOut,
            )
          ],
        ),
        body: new TabBarView(children: [
          _getFaceScreen(context),
        ]),
      ),
    );
  }

  List<DropdownMenuItem<int>> _getCameras() {
    List<DropdownMenuItem<int>> cameraItems = [];

    cameraItems.add(new DropdownMenuItem(
      child: new Text('BACK'),
      value: FlutterMobileVision.CAMERA_BACK,
    ));

    cameraItems.add(new DropdownMenuItem(
      child: new Text('FRONT'),
      value: FlutterMobileVision.CAMERA_FRONT,
    ));

    return cameraItems;
  }

  List<DropdownMenuItem<Size>> _getPreviewSizes(int facing) {
    List<DropdownMenuItem<Size>> previewItems = [];

    List<Size> sizes = FlutterMobileVision.getPreviewSizes(facing);

    if (sizes != null) {
      sizes.forEach((size) {
        previewItems.add(
          new DropdownMenuItem(
            child: new Text(size.toString()),
            value: size,
          ),
        );
      });
    } else {
      previewItems.add(
        new DropdownMenuItem(
          child: new Text('Empty'),
          value: null,
        ),
      );
    }
    return previewItems;
  }

  Widget _getFaceScreen(BuildContext context) {
    List<Widget> items = [];

    items.add(new Padding(
      padding: const EdgeInsets.only(
        top: 8.0,
        left: 18.0,
        right: 18.0,
      ),
      child: const Text("Camera:"),
    ));

    items.add(new Padding(
      padding: const EdgeInsets.only(
        left: 18.0,
        right: 18.0,
      ),
      child:  new TextFormField(
        decoration: new InputDecoration(labelText: 'Titre'),
        validator: (value) => value.isEmpty ? 'Titre manquant !': null,
        onSaved: (value) => _title = value,
      )
    ));


    items.add(new Padding(
      padding: const EdgeInsets.only(
        left: 18.0,
        right: 18.0,
      ),
      child: new DropdownButton(
        items: _getCameras(),
        onChanged: (value) {
          _previewFace = null;
          if (value == FlutterMobileVision.CAMERA_FRONT) {
            setState(() => _torchFace = false);
          }
          setState(() => _cameraFace = value);
        },
        value: _cameraFace,
      ),
    ));

    items.add(new Padding(
      padding: const EdgeInsets.only(
        top: 8.0,
        left: 18.0,
        right: 18.0,
      ),
      child: const Text("Taille de l'image:"),
    ));

    items.add(new Padding(
      padding: const EdgeInsets.only(
        left: 18.0,
        right: 18.0,
      ),
      child: new DropdownButton(
        items: _getPreviewSizes(_cameraFace),
        onChanged: (value) {
          setState(() => _previewFace = value);
        },
        value: _previewFace,
      ),
    ));


    items.add(new SwitchListTile(
      title: const Text('Lampe:'),
      value: _torchFace,
      onChanged: (value) => setState(() {
        if (_cameraFace == FlutterMobileVision.CAMERA_BACK) {
          _torchFace = value;
        }
      }),
    ));


    items.add(
      new Padding(
        padding: const EdgeInsets.only(
          left: 18.0,
          right: 18.0,
          bottom: 12.0,
        ),
        child: new RaisedButton(
          onPressed: _face,
          child: new Text('Photo !'),
        ),
      ),
    );

    items.addAll(
      ListTile.divideTiles(
        context: context,
        tiles: _faces
            .map(
              (face) => new FaceWidget(face),
        )
            .toList(),
      ),
    );

    return new ListView(
      padding: const EdgeInsets.only(
        top: 12.0,
      ),
      children: items,
    );
  }

  Future<Null> _face() async {
    List<Face> faces = [];
    try {
      faces = await FlutterMobileVision.face(
        flash: _torchFace,
        autoFocus: _autoFocusFace,
        multiple: _multipleFace,
        showText: _showTextFace,
        preview: _previewFace,
        camera: _cameraFace,
        fps: 15.0,
      );
    } on Exception {
      faces.add(new SmileyFace(-1));
    }

    if (!mounted) return;

    List<SmileyFace> smileyInfo = new List<SmileyFace>();
    SmileyFace smileyFace = new SmileyFace(0);
    double moyenne = 0;
    int nombreDePersonnes = faces.length;
    if (nombreDePersonnes != 0) {
      double somme = 0;
      faces.forEach((face) {
        if (face.smilingProbability != -1) {
          somme += (face.smilingProbability * 100).round();
        }
      });
      moyenne = somme / faces.length;
    }
    smileyFace.nombre_de_personnes = nombreDePersonnes;
    smileyFace.moyenne = moyenne;
    smileyFace.title = _title;
    smileyInfo.add(smileyFace);
    setState(() => _faces = smileyInfo);
  }


  void _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    }
    catch (e){
      print(e);
    }
  }

}


