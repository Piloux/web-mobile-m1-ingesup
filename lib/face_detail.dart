import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';

import 'face_class.dart';

class FaceDetail extends StatefulWidget {
  final SmileyFace face;

  FaceDetail(this.face);

  @override
  _FaceDetailState createState() => new _FaceDetailState();
}

class _FaceDetailState extends State<FaceDetail> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Smiley'),
      ),
      body: new ListView(
        children: <Widget>[
          new ListTile(
            title: new Text(widget.face.nombre_de_personnes.toString()),
            subtitle: const Text("Nombre de personnes"),
          ),
          new ListTile(
            title: new Text(widget.face.moyenne.toString()),
            subtitle: const Text('Taux de satisfaction'),
          ),
        ],
      ),
    );
  }
}
