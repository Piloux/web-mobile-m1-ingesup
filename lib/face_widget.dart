import 'package:flutter/material.dart';
import 'package:flutter_mobile_vision/flutter_mobile_vision.dart';

import 'face_detail.dart';
import 'face_class.dart';

class FaceWidget extends StatelessWidget {
  final SmileyFace face;

  FaceWidget(this.face);

  @override
  Widget build(BuildContext context) {
    this.face.title = (this.face.title != null) ? this.face.title : "";
    return new ListTile(
      leading: const Icon(Icons.face),
      title: new Text(this.face.title),
      trailing: const Icon(Icons.arrow_forward),
      onTap: () => Navigator.of(context).push(
        new MaterialPageRoute(
          builder: (context) => new FaceDetail(face),
        ),
      ),
    );
  }
}