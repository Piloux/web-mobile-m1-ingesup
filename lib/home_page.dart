import 'dart:io';

import 'package:flutter/material.dart';
import 'auth.dart';

class HomePage extends StatefulWidget{
  HomePage({this.auth, this.onSignedOut});
  final BaseAuth auth;
  final VoidCallback onSignedOut;

  @override
  _HomePage createState(){
    return new _HomePage();
  }
}


class _HomePage extends State<HomePage> {


  void _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    }catch (e){
      print(e);
    }
  }


  @override
   Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Smiley'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Deconnexion', style: new TextStyle(fontSize: 17.0, color: Colors.white)),
            onPressed: _signOut,
          )
        ],
      ),
    );
  }
}