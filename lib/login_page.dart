import 'package:flutter/material.dart';
import 'auth.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'dart:core';


class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedIn});
  final BaseAuth auth;
  final VoidCallback onSignedIn;
  @override
  State<StatefulWidget> createState() =>  new _LoginPageState();
}

enum FormType {
  login,
  register
}

class _LoginPageState extends State<LoginPage> {

  final formKey = new GlobalKey<FormState>();

  String _email;
  String _password;
  FormType _formType = FormType.login;

  bool validateAndSave(){
    final form = formKey.currentState;
    if(form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void validateAndSubmit() async{
    if(validateAndSave()){
      try{
        if(_formType == FormType.login){
          String userId = await widget.auth.signInWithEmailAndPassword(_email, _password);
          print('signed in: $userId');
        }
        else{
          String userId = await widget.auth.createUserWithEmailAndPassword(_email, _password);
          print('register user: $userId');
        }
        widget.onSignedIn();
      }
      catch(e){
        print('error: $e');
      }
    }
  }

  void moveToRegister(){
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });

  }

  void moveToLogin(){
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }
  
  @override
    Widget build(BuildContext context) {
      return new Scaffold(
        appBar: new AppBar(
          title: new Text('Smiley'),
        ),
        resizeToAvoidBottomPadding: false,
        body:
        new Container(
          padding: EdgeInsets.all(16.0),
          child: new Form(
            key: formKey,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: buildInputs() + buildSubMitButtons() + <Widget>[
                Expanded(
                  child: FlareActor("assets/loading.flr", animation: 'load')
                ),
              ],
            ),
          ),
        )
      );
  }

  List<Widget> buildInputs() {
    return [
      new TextFormField(
        decoration: new InputDecoration(labelText: 'Email'),
        //validator: (value) => value.isEmpty ? 'Email manquant !': null,
        validator: (value) {
          if (value.isEmpty || EmailValidator.validate(value) == false) {
            return ('Email non valide ou vide');
          }
        },
        onSaved: (value) => _email = value,
      ),
      new TextFormField(
        decoration: new InputDecoration(labelText: 'Password'),
        obscureText: true,
        validator: (value) {
          if (value.isEmpty || value.length < 8) {
            return ('Password vide ou trop court (min 8 caracteres)');
          }
        },
        //validator: (value) => value.isEmpty ? 'password manquant !': null,
        onSaved: (value) => _password = value,
      ),
    ];
  }


  List<Widget> buildSubMitButtons() {
    if (_formType == FormType.login) {
      return [
        new RaisedButton(
          child: new Text('Connexion', style: new TextStyle(fontSize: 20.0),),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text(
              'Créer un compte', style: new TextStyle(fontSize: 20.0)),
          onPressed: moveToRegister,
        )
      ];
    }
    else {
      return [
        new RaisedButton(
          child: new Text('Créer un compte', style: new TextStyle(fontSize: 20.0),),
          onPressed: validateAndSubmit,
        ),
        new FlatButton(
          child: new Text(
              'Deja un compte ? Connecte toi ', style: new TextStyle(fontSize: 20.0)),
          onPressed: moveToLogin,
        )
      ];
    }
  }
}