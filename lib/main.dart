import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'root_page.dart';
import 'auth.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  String textValue = 'Bienvenue sur smiley ';
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  @override
  void initState() {
    firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg){
        print("on launchcalled");
      },
      onResume: (Map<String, dynamic> msg){
        print("on resume called");
      },
      onMessage: (Map<String, dynamic> msg){
        print("on message called");
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true,
            alert: true,
            badge: true
        )
    );
    firebaseMessaging.onIosSettingsRegistered.listen((IosNotificationSettings setting){
      print('IOS setting Registed');
    });
    firebaseMessaging.getToken().then((token){
      update(token);
    });
  }
  update(String token){
    print(token);
    textValue = token;
    setState(() {

    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Smiley',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new RootPage(auth: new Auth())
    );
  }
}


